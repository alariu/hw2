package com.mycompany.l02;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class SearchArticleTestPositive extends TestBase {


    @Test
    public void searchArticle() {
        //Printing Id of the thread on using which test method got executed
        System.out.println("Test Case One with Thread Id:- "
                + Thread.currentThread().getId());

        WebElement searchBar = driver.findElement(By.name("search"));
        searchBar.clear();
        searchBar.sendKeys("32525737\n");

        driver.findElement(By.linkText("Кабель Apple USB-Lightning (MD819ZM/A), белый, 2м")).click();

        assertTrue(isElementPresent(By.cssSelector("div[data-widget='webProductHeading'] .b3a8")));

    }


}
