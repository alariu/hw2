package com.mycompany.l02;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.openqa.selenium.By.cssSelector;


public class BasicCommandsTest extends TestBase {


    @BeforeClass
    public void checkAttributesTests() {
        WebElement searchBar = driver.findElement(By.name("search"));
        searchBar.clear();
        searchBar.sendKeys("32525737\n");
        driver.findElement(By.linkText("Кабель Apple USB-Lightning (MD819ZM/A), белый, 2м")).click();
    }

    @Test
    public void checkBrandName() {
        String brandName = driver.findElement(By.linkText("Apple")).getText();
        Assert.assertEquals(brandName, "Apple");
        System.out.println("Brand name is "+ brandName);

    }

    @Test
    public void checkArticlePrice () {
        String articlePrice = driver.findElement(cssSelector("div[data-widget='webPrice'] .c2h5.c2h6")).getText().replaceAll("&nbsp;", "");
        Assert.assertEquals(articlePrice, "2 481 ₽ ");
        System.out.println("Article prise is "+ articlePrice);
    }

    @Test
    public void checkPriceColour () {
        String priceColour = driver.findElement(cssSelector("div[data-widget='webPrice'] .c2h5.c2h6")).getCssValue("color");
        Assert.assertEquals(priceColour, "rgba(249, 17, 85, 1)");
        System.out.println("Prise colour is " + priceColour);
    }




}
